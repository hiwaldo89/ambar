"use strict";

(function ($) {
  // Hamburger menu
  $('#menu-open').click(function (e) {
    e.preventDefault();
    $(this).addClass('is-active');
    $('#menu-close').addClass('is-active');
    $('.ambar-navigation').addClass('ambar-navigation--opened');
  });
  $('#menu-close').click(function (e) {
    e.preventDefault();
    $(this).removeClass('is-active');
    $('#menu-open').removeClass('is-active');
    $('.ambar-navigation').removeClass('ambar-navigation--opened');
  }); // Slick sliders

  $('.square-img-wrapper__slider').slick({
    arrows: false,
    fade: true
  });
  $('.text-slider').slick({
    arrows: false,
    fade: true
  });
  $('.lighting-icon').click(function () {
    var index = $(this).data('slide');
    $('.lighting-icon').removeClass('lighting-icon--active');
    $(this).addClass('lighting-icon--active');
    $('.square-img-wrapper__slider').slick('slickGoTo', index);
    $('.text-slider').slick('slickGoTo', index);
  });
  $('.ambient-lighting-slider').slick({
    arrows: false,
    autoplay: true,
    fade: true
  });
  $('.slick-slide').click(function () {
    var slider = $(this).parents('.slick-slider');
    $(slider).slick('slickNext');
  });
})(jQuery);