<?php
/**
 * Template Name: Home
 */
get_header(); ?>
    <div class="p-home">
        <div class="square-img-wrapper bg-gray">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/diamond-1.svg" alt="diamante" class="diamond">
            <div class="container pb-5">
                <div class="square-img-wrapper__inner">
                    <!-- <span class="diamond"></span> -->
                    <div class="square-img-wrapper__slider">
                        <?php $slider = get_field('slider'); ?>
                        <?php foreach( $slider as $slide ) : ?>
                            <img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" class="img-fluid">
                        <?php endforeach; ?>
                    </div>
                </div>
                <p class="text-center text-gold h3 mt-5 mb-4">
                    <?php the_field('headline'); ?>
                </p>
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <p class="text-gray mb-0">
                            <?php the_field('nosotros'); ?>
                        </p>
                        <div class="text-center mt-4 mb-5 d-lg-none">
                            <a href="<?php the_field('catalogo'); ?>" target="_blank" class="ambar-btn">Descargar catálogo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <?php if(have_rows('iluminacion_bloques')) : while(have_rows('iluminacion_bloques')) : the_row(); ?>
                <div class="row align-items-center bg-gray pb-5 pb-lg-0">
                    <div class="col-lg-6 px-lg-0<?php if(get_row_index()&1) : ?> order-1 order-lg-0<?php else : ?> order-1<?php endif; ?>">
                        <div class="ambient-lighting-slider">
                            <?php $blockSlider = get_sub_field('galeria'); ?>
                            <?php foreach($blockSlider as $slide) : ?>
                                <img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" class="img-fluid">
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 text-center<?php if(get_row_index()&1) : ?> order-0 order-lg-1<?php else : ?> order-0<?php endif; ?>">
                        <div class="row py-5">
                            <div class="col-lg-9 mx-lg-auto">
                                <h2 class="h3 text-gold mb-5"><?php the_sub_field('titulo'); ?></h2>
                                <div class="text-gray mb-0">
                                    <?php the_sub_field('descripcion'); ?>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="play-btn mx-auto mt-5" data-toggle="modal" data-target="#light-modal-<?php echo get_row_index(); ?>">
                                        <i class="fas fa-play"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
        <?php if(have_rows('contact_banner')) : while(have_rows('contact_banner')) : the_row(); ?>
            <div class="contact-banner" style="background-image: url(<?php echo get_sub_field('imagen')['url']; ?>);">
                <div class="container">
                    <div class="row">
                        <div class="col-11 col-md-8 col-lg-6 mx-auto">
                            <a href="<?php echo get_permalink( get_page_by_path( 'contacto' ) ); ?>" class="contact-banner__button">
                                <div class="contact-banner__box py-5">
                                    <p class="mb-0 text-center py-5">
                                        <?php the_sub_field('texto'); ?>
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
<?php get_footer(); ?>