<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <?php wp_head(); ?>
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="7096e93a-89ea-493a-855d-e63ddef6cf7a";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>
<body>
    <div class="ambar-navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 pt-3 mt-lg-5 pt-lg-5">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/stamp-light.svg" alt="Ámbar iluminación" class="ambar-navigation__stamp mb-5 mb-lg-0">
                </div>
                <div class="col-lg-8">
                    <?php wp_nav_menu(array(
                        'menu' => 'menu-1',
                        'container' => false
                    )); ?>
                    <!-- <p class="text-gold">Calle Técnicos 54, col. San Pedrito Peñuelas. Querétaro</p> -->
                    <p class="text-gold">TEL: (442) 361 6520</p>
                    <ul class="social-links mt-5">
                        <li>
                            <a href="https://es-la.facebook.com/iluminacionambar/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/ambariluminacion/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="fas fa-envelope"></i>
                            </a>
                        </li>
                    </ul>
                    <button id="menu-close" class="hamburger hamburger--slider is-active" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <section class="welcome-section">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/stamp.svg" alt="Ámbar iluminación" class="stamp d-none d-lg-block">
        <div class="container">
            <div class="row align-items-center py-4 py-lg-0">
                <div class="col-8 col-lg-6 ml-lg-auto">
                    <h1 class="text-lg-center text-white"><?php the_title(); ?></h1>
                    <?php if(is_front_page()) : ?>
                        <div class="text-center mt-4 d-none d-lg-block">
                            <a href="<?php the_field('catalogo'); ?>" target="_blank" class="ambar-btn">Descargar catálogo</a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-4 col-lg-3 text-right">
                    <button id="menu-open" class="hamburger hamburger--slider" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </section>