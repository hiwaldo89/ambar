<?php
/**
 * Template Name: Events
 */
get_header(); ?>
    <div class="p-events">
        <div class="bg-gray services-listing pb-5">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/diamond.svg" alt="diamante" class="diamond">
            <div class="container pb-5">
                <div class="row pb-5">
                    <div class="col-lg-6 ml-lg-auto text-gold pb-5">
                        <ul>
                            <li>BODAS</li>
                            <li>GRADUACIONES</li>
                            <li>CORPORATIVOS</li>
                            <li>CONCIERTO</li>
                            <li>DE DÍA</li>
                            <li>DE NOCHE</li>
                            <li>INTERIORES</li>
                            <li>EXTERIORES</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="square-img-wrapper">
                <div class="container pb-5">
                    <div class="square-img-wrapper__inner">
                        <div class="square-img-wrapper__slider">
                            <?php $slider = get_field('slider'); ?>
                            <?php foreach( $slider as $slide ) : ?>
                                <img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" class="img-fluid">
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>