<?php
/**
 * Template Name: Events
 */
get_header(); ?>
    <div class="p-events">
        <div class="pb-5">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/diamond.svg" alt="diamante" class="diamond">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mx-lg-auto text-white pb-5">
                        <div class="text-center mb-5">
                            <?php the_field('welcome_text'); ?>
                        </div>
                        <h2 class="text-gold text-center pt-5">Iluminación arquitectónica</h2>
                        <h3 class="text-gold text-center pt-3 mb-5">Luminarias</h3>
                        <div class="row">
                            <div class="col-lg-9 mx-lg-auto">
                                <div class="row">   
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon lighting-icon--active" data-slide="0">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/1-movil-wash.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="1">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/2-color-city.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="2">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/3-par-led.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="3">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/4-pin-spots-inalambrico.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="4">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/5-pin-spots-alambrico.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="5">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/6-comet-head.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="6">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/7-extensiones-foco.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 mb-4">
                                        <div class="px-lg-2">
                                            <div class="lighting-icon" data-slide="7">
                                                <?php echo file_get_contents(get_bloginfo('template_url') . "/assets/img/8-cabeza-movil-spot.svg"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="square-img-wrapper">
                <div class="container pb-5">
                    <div class="row">
                        <div class="col-lg-8 mx-lg-auto">
                            <div class="text-slider mb-4">
                                <?php $textSlider = get_field('text_slider'); ?>
                                <?php foreach( $textSlider as $slide ) : ?>
                                    <div class="text-center">
                                        <h2 class="text-gold"><?php echo $slide['titulo']; ?></h2>
                                        <div class="text-white"><?php echo $slide['contenido']; ?></div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="square-img-wrapper__inner">
                                <div class="square-img-wrapper__slider">
                                    <?php $slider = get_field('slider'); ?>
                                    <?php foreach( $slider as $slide ) : ?>
                                        <img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>" class="img-fluid">
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>