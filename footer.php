    <footer class="ambar-footer bg-gray py-5">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/stamp-footer.svg" alt="Ámbar iluminación" class="ambar-footer__stamp">
        <div class="container text-center text-gold py-5 position-relative">
            <p class="h2">LIGHT UP THE DARKNESS</p>
            <p class="h5">BOB MARLEY</p>
            <ul class="social-links mt-5 text-center">
                <li>
                    <a href="https://es-la.facebook.com/iluminacionambar/" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/ambariluminacion/" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="mailto:info@iluminacionambar.com" target="_blank">
                        <i class="fas fa-envelope"></i>
                    </a>
                </li>
            </ul>
            <!-- <p class="mt-5">Calle Técnicos 54, col. San Pedrito Peñuelas. Querétaro</p> -->
            <div class="d-flex mt-5 align-items-center">
                <div class="ml-auto mr-5">
                    <a href="https://www.bodas.com.mx/decoracion-para-bodas/ambar-iluminacion--e156746" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/bodas-ambar.svg" alt="Bodas.com.mx" class="img-fluid" width="190"></a>
                </div>
                <div class="mr-auto">
                    <a href="https://www.zankyou.com.mx/f/ambar-iluminacion-552774" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/img/zankyou-ambar.svg" alt="Zankyou" class="img-fluid" width="150"></a>
                </div>
            </div>
        </div>
        <div class="copyright container text-center mt-5">
            MADE WITH LOVE BY <a href="https://somosbloom.com" target="_blank">BLOOM</a>
        </div>
    </footer>
    <?php if(is_front_page()) : ?>
        <?php if(have_rows('iluminacion_bloques')) : while(have_rows('iluminacion_bloques')) : the_row(); ?>
            <div class="modal fade" id="light-modal-<?php echo get_row_index(); ?>">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body p-0">
                            <div class="embed-responsive embed-responsive-16by9">
                                <?php the_sub_field('video'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    <?php endif; ?>
    <?php wp_footer(); ?>
</body>
</html>