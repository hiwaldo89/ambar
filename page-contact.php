<?php
/**
 * Template Name: Contact
 */
get_header(); ?>
    <div class="p-contact">
        <div class="bg-gray services-listing pb-5">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/diamond-2.svg" alt="diamante" class="diamond">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mx-auto pb-5">
                        <h2 class="text-gold text-center">DAMOS VIDA A TU EVENTO</h2>
                        <p class="text-gold text-center h2 mb-5">CONTÁCTANOS</p>
                        <?php gravity_form(1, false, false, false, false); ?>
                    </div>
                </div>
                <p class="text-center text-gold h3 mt-5">
                    LLÁMANOS: (442) 361 6520
                </p>
            </div>
        </div>
    </div>
<?php get_footer(); ?>