<?php

function ambar_setup() {
    // Make theme available for translation.
    load_theme_textdomain( 'ambar', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
    
    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );

    // Enable support for Post Thumbnails on posts and pages.
    add_theme_support( 'post-thumbnails' );

    // Register menus
    register_nav_menus( array(
        'menu-1' => esc_html__( 'Primary', 'ambar' ),
    ) );

    // Switch default core markup for search form, comment form, and comments to output valid HTML5.
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
}
add_action( 'after_setup_theme', 'ambar_setup' );

// Enqueue scripts and styles.
function ambar_scripts() {
    wp_enqueue_style( 'bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' );
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap' );
    wp_enqueue_style( 'ambar-style', get_stylesheet_uri() );

    wp_enqueue_script( 'fontawesome', 'https://pro.fontawesome.com/releases/v5.7.2/js/all.js', '', '', true );
    wp_enqueue_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/assets/js/vendor.js', array('jquery'), '', true );
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'ambar_scripts' );

// GravityForms compatibility file
require_once get_template_directory() . '/inc/ambar-gravityforms.php';